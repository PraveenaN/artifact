pipeline{
    agent any

    stages{
        stage('Hello World'){
            steps{
                echo 'Hello, World!'
            }
        }
        stage('Create text file'){
            steps{
                script{
                     sh "echo 'Hello' > hello.txt"
                }
               
            }
        }
        stage('Archive Artifacts'){
            steps{
                archiveArtifacts 'hello.txt'
            }
        }
    }
}